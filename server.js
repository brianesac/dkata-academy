'use strict';
const Joi = require('@hapi/joi')
const Hapi = require('@hapi/hapi')
const handler = require('./handler/handler')
const Inert = require('inert');
const Vision = require('vision');
const HapiSwagger = require('hapi-swagger');

const init = async () => {
    const server = Hapi.server({
        port: 5000,
        host: 'localhost',
    });

    const swaggerOptions = {
        info: {
            title: 'Tasks API Documentation',
            version: '0.0.1',
        }
    };

    await server.register([
        Inert,
        Vision,
        {
            plugin: HapiSwagger,
            options: swaggerOptions
        }
    ]);

    server.route([
        {
            method: 'GET',
            path: '/task/{id}',
            handler: handler.getTask,
            options:{
                validate: {
                    params: {
                        id: Joi.number().integer()
                    }
                }
            },
            options: {
                description: 'Get a task',
                notes: 'Return a task specified by id',
                tags: ['api'],
            },
           
        },
        {
            method: 'GET',
            path: '/task',
            handler: handler.getTasks,
            options: {
                description: 'Get all tasks',
                notes: 'Return all tasks',
                tags: ['api'],
            }
        },
        {
            method: 'POST',
            path: '/task',
            handler: handler.postTask,
            options: {
                validate: {
                    payload: {
                        id: Joi.forbidden(),
                        title: Joi.string().required(),
                        description: Joi.string().required(),
                        status: Joi.forbidden(),
                        comment: Joi.string(),
                        dueDate: Joi.date()
                    }
                }
            },
            options: {
                description: 'Post a task',
                notes: 'Post a task',
                tags: ['api'],
            },
        },
        {
            method: 'PUT',
            path: '/task/{id}',
            handler: handler.updateTask,
            options: {
                validate: {
                    payload: {
                        id: Joi.forbidden(),
                        title: Joi.string(),
                        description: Joi.string(),
                        status: Joi.string().valid('complete', 'in progress'),
                        comment: Joi.string(),
                        dueDate: Joi.date()
                    }
                }
            },
            options: {
                description: 'Update a task',
                notes: 'Update a task specified by id',
                tags: ['api'],
            },

        },
        {
            method: 'DELETE',
            path: '/task/{id}',
            handler: handler.deleteTask,
            options: {
                validate: {
                    params: {
                        id: Joi.number().integer(),
                    }
                }
            },
            options: {
                description: 'Delete a task',
                notes: 'Delete a task specified by id',
                tags: ['api'],
            },

        }
    ]);
    server.start()
    console.log('Server running at: ', server.info.uri)
    return server
}
init()
module.exports = { init }