const mongoose = require('mongoose')
const connectDB = async () => {
    await mongoose.connect('mongodb://localhost/tasksDb', {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
        useFindAndModify: false
    });
    mongoose.set('debug', true)
}
connectDB()

const Task = mongoose.model('Task', {
    id: Number,
    title: String,
    description: String,
    dueDate: Date,
    comment: String,
    status: String,
    createdAt: Date,
    updatedAt: Date
});

const getTask = function (request, h) {
    return Task.find({ "id": request.params.id }).lean()
}

const getTasks = function (request, h) {
    const sortBy = request.query.sort
    const limit = parseInt(request.query.limit)
    const offset = parseInt(request.query.offset)
    const status = request.query.status
    const date = request.query.date
    if (!sortBy && !limit && !offset && !status && !date) {
        return Task.find()
    }

    if (status === 'complete') {
        return h.response({ "message": "it has been completed, you can't see it" })
    }
    if (status && date) {
        return Task.find({ $and: [{ status: status }, { updatedAt: date }] }).sort({ sortBy: 1 }).limit(limit).skip(offset)
    }
    return Task.find({ status: status }).sort({ sortBy: 1 }).limit(limit).skip(offset)
}

const postTask = async function (request, h) {
    let maxId = await Task.aggregate([{ $group: { _id: "id", max: { $max: "$id" } } }])
    let nextId = 0
    if (maxId === null || maxId === undefined) {
        nextId = 1
    }
    nextId = maxId[0].max + 1
    let theComment = ""
    // let myDate = { $date: request.payload.dueDate }
    request.payload.comment === undefined || request.payload.comment === null ? theComment : theComment = request.payload.comment
    Object.assign(request.payload, { id: nextId }, { comment: theComment }, { createdAt: new Date() }, { updatedAt: new Date() }, { status: "new" })
    await Task.insertMany([request.payload])
    return h.response(request.payload).code(201)
}

const updateTask = async function (request, h) {
    let id = request.params.id
    let filter = { "id": id }
    let update = request.payload
    let taskFound = await Task.find(filter)
    if (taskFound[0].status === 'complete') {
        return h.response({ "message": "it has been completed, you can't comment it" })
    }
    request.payload.updatedAt = new Date()
    await Task.updateOne(filter, update, { new: true })
    let result = Task.find(filter, update)
    return result
}

const deleteTask = async function (request, h) {
    let id = request.params.id
    let filter = { "id": id }
    let process = await Task.deleteOne(filter)
    if(process){
        return h.response({"message":"deleted"})
    }
    return h.response({"message":"failed"})
}

module.exports = { postTask, updateTask, getTask, getTasks, deleteTask }
